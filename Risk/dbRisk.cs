﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Risk
{
    class dbRisk : DbContext
    {
        //Crea el context. Crea una DB per defecte (es a dir, MS SQL Server, amb nom "Test")
        public dbRisk() : base("Riskdatabase")
        {

        }

        //Mapeig als POCOS.
        public DbSet<Jugador> Jugador { get; set; }
        public DbSet<Region> Region { get; set; } 
         public DbSet<Continent> Continent { get; set; } 
       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ProvaContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<dbRisk>());  //equivaldria a un create

            base.OnModelCreating(modelBuilder);
        }

    }
}
