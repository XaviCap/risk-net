﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;  //import per les anotacions
using System.ComponentModel.DataAnnotations;
namespace Risk
{
    public class Jugador
    {
        public int JugadorID { get; set; }
        [Required]
        private String Nom { get; set; }
        [Required]
        private int Ordretirada { get; set; }
        [Required]
        public virtual ICollection<Region> Regiones { get; set; }  //per a inferir ha de ser el nom de l'altra classe amb s al final

        public int NumeroVictories { get; set; }

        public int NumeroRegions { get; set; }

        public Jugador(String nom, int ordre) {
            this.Nom = nom;
            this.Ordretirada = ordre;
            this.NumeroVictories = 0;
            this.NumeroRegions = 0;
                 }
    }
}
