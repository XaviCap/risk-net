﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;  //import per les anotacions
using System.ComponentModel.DataAnnotations;
namespace Risk
{
    public class Continent
    {
        public int ContinentID { get; set; }
        [Required]
        public String nom { get; set; }
        [Required]
        //relacio n a 1, inferida per el sistema
        public Jugador Propietari { get; set; }
        [Required]
        public int bonus;
        public virtual ICollection<Region> Regiones { get; set; }

        public Continent(String nom)
        {
            this.nom = nom;
            this.Propietari = null;
            this.bonus = 10;
        }
    }
}