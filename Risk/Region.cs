﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;  //import per les anotacions
using System.ComponentModel.DataAnnotations;

namespace Risk
{
    public class Region
    {
        [Required]
        private String Nom { get; set; }
     
        //relacio n a 1, inferida per el sistema
        public Jugador Propietari { get; set; }
        [Required]
        //relacio n a n, inferida per el sistema
        public virtual ICollection<Region> Regionsveines { get; set; } //per a inferir ha de ser el nom de l'altra classe amb s al final //set de regions veines

        public int Tropes { get; set; }

        public Region(String nom,int tropes) {
            this.Nom = nom;
            this.Tropes = tropes;
        }
    } }