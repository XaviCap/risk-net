﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    class init
    {



        static void Main(string[] args)
        {
            using (var ctx = new dbRisk()) {
                //crear regiones
                var Dalaran = new Region("Dalaran",10);
                var Gnomeregan = new Region("Gnomeregan", 10);
                var Lunargenta = new Region("Lunargenta", 10);
                var Orgrimmar = new Region("Orgrimmar", 10);
                var Ventormenta = new Region("Ventormenta", 10);
                var Entrañas = new Region("Entrañas", 10);
                //crear continentes
                var Kalimdor = new Continent("Kalimdor");
                var ReinosdelEste = new Continent("Reinos del Este");

                // crear jugadores

                var Player1 = new Jugador("Sylvanas Brisaveloz", 0);
                var Player2 = new Jugador("Anduin Wrynn", 0);
                //
               
                //añadir cosas al context
                ctx.Region.Add(Dalaran);
                ctx.Region.Add(Gnomeregan);
                ctx.Region.Add(Lunargenta);
                ctx.Region.Add(Orgrimmar);
                ctx.Region.Add(Ventormenta);
                ctx.Region.Add(Entrañas);
                ctx.Continent.Add(Kalimdor);
                ctx.Continent.Add(ReinosdelEste);
                ctx.Jugador.Add(Player1);
                ctx.Jugador.Add(Player2);

                //asignar regiones a continentes
                ReinosdelEste.Regiones.Add(Lunargenta);
                ReinosdelEste.Regiones.Add(Orgrimmar);
                ReinosdelEste.Regiones.Add(Entrañas);
                Kalimdor.Regiones.Add(Ventormenta);
                Kalimdor.Regiones.Add(Gnomeregan);
                Kalimdor.Regiones.Add(Dalaran);

                //asignar regiones a jugador
                Player1.Regiones.Add(Lunargenta);
                Player1.Regiones.Add(Orgrimmar);
                Player1.Regiones.Add(Entrañas);

                
                List <Region> listareg1 = Player1.Regiones.ToList();
                
                foreach(Region r in listareg1){
                    Player1.NumeroRegions++;
                     }

                Player2.Regiones.Add(Ventormenta);
                Player2.Regiones.Add(Gnomeregan);
                Player2.Regiones.Add(Dalaran);

                List<Region> listareg2 = Player2.Regiones.ToList();

                foreach (Region r in listareg2)
                {
                    Player2.NumeroRegions++;
                }
                ctx.SaveChanges();
            }
        } }
}
